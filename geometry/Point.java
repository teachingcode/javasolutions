
package geometry;

/**
 * A class describing points in 2D.
 * @author lct495
 */
public class Point {
    // A point is identified by its x and y coordinates
    double x;
    double y;

    /**
     * Default constructor. 
     * I chose to make the default constructor
     * construct the origin point (0,0).
     */
    public Point() {
        this.x = 0;
        this.y = 0;
    }
    
    /**
     * Creates a point given its coordinates
     * @param x
     * @param y 
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
    /**
     * Returns the x coordinate
     * @return 
     */
    public double getX() {
        return x;
    }
    /**
     * Sets the x coordinate
     * @param x 
     */
    public void setX(double x) {
        this.x = x;
    }
    /**
     * Returns the y coordinate
     * @return 
     */
    public double getY() {
        return y;
    }
    /**
     * Sets the y coordinate
     * @param y 
     */
    public void setY(double y) {
        this.y = y;
    }
    /**
     * Prints info about the point.
     */
    public void print(){
        System.out.println("Point of coordinates ("+x+","+y+")");
    }
    
    
}
