package geometry;

/**
 * A class representing rectangular objects.
 * @author lct495
 */
public class Rectangle {
    // A rectangle is characterized by 
    // its position, with and height
    // I arbitrarily decided to use
    // an Point object to represent the
    // position. Alternatively, one might
    // thing of using the x and y coordinates
    double height;
    double width;
    Point position;
    
    /**
     * Empty constructor.
     */
    public Rectangle(){
        this.height = 0;
        this.width = 0;
        // Check out the constructor of Point
        // the default constructor corresponds 
        // to the origin (0,0).
        this.position = new Point();
    }
    /**
     * Constructor given height and width.
     * The position will be (0,0);
     * @param height
     * @param width 
     */
    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
        this.position = new Point();
    }
    /**
     * Constructor given all its elements.
     * @param height
     * @param width
     * @param position 
     */
    public Rectangle(double height, double width, Point position) {
        this.height = height;
        this.width = width;
        this.position = position;
    }
    // Getters and setters
    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }
    /**
     * Prints a summary 
     */
    public void print(){
        // Note how an object can call its own method getArea() and getPerimeter()
        // Alternatively, one can use this.getArea() and this.getPerimeter()
        // Notice also that it does not matter that the methods are defined below:
        // the class sees them anyway. 
        System.out.println("Rectangle with width "+width+", height "+height+", position ("+position.getX()+","+position.getY()+"), area "+getArea()+" perimeter "+getPerimeter());
    }
    /**
     * Calculates and returns the area of the rectangle.
     * @return 
     */
    public double getArea(){
        return width * height;
    }
    /**
     * Calculates and returns the perimeter of the rectangle. 
     * @return 
     */
    public double getPerimeter(){
        return 2 * (width + height);
    }
    /**
     * Checks if the rectangle is empty.
     * @return 
     */
    public boolean isEmpty(){
        boolean isEmpty = false;
        if(getArea() == 0){
            isEmpty = true;
        }
        return isEmpty;
        // Notice the difference between logical equal (==)
        // and assignment equal (=). Notice that this code could have
        // been written in a much more compact way:
        // simply "return getArea() == 0;"
    }
    public void scale(double scalar){
        this.height = this.height * scalar;
        this.width = this.width * scalar;
        
        // Note that the "this." is redundant: 
        // the code would work correctly also without
        // it. It is just good practice to use
        // "this" when confusion might arise
        // regarding which variable you are referring to.
        // Look for example the method setWidth(): there
        // using "this" is necessary. 
    }
    public boolean hasBiggerArea(Rectangle r){
        return r.getArea() > this.getArea();
        // Note that (r.getArea() > this.getArea();) 
        // evaluates in a boolean value. A more verbose way:
        // if(r.getArea() > this.getArea()){
        //     return true;
        // }else{
        //     return false;
        // }
    }
}
