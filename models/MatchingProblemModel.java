/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.cplex.IloCplex;
import problems.MatchingProblem;

/**
 *
 * @author lct495
 */
public class MatchingProblemModel {
    
    private final IloCplex model;
    private final MatchingProblem problem;
    private final IloIntVar[][] x;
    
    public MatchingProblemModel(MatchingProblem problem) 
        throws IloException{
        this.model = new IloCplex();
        this.problem = problem;
        
        // Creates the decision variables
        this.x = new IloIntVar[problem.getnPeople()][problem.getnPeople()];
        for(int i = 1; i <= problem.getnPeople()-1; i++){
            for(int j = i+1; j <= problem.getnPeople(); j++){
                
                    x[i-1][j-1] = model.boolVar("x_"+i+"_"+j);
                
            }   
        }
        
        // Creates the objective function
        IloLinearNumExpr objective = model.linearNumExpr();
        for(int i = 1; i <= problem.getnPeople()-1; i++){
            for(int j = i+1; j <= problem.getnPeople(); j++){
                objective.addTerm(problem.getQuality(i, j), x[i-1][j-1]);
            }   
        }
        model.addMaximize(objective);
        
        // Creates the constraints
        for(int i = 1; i <= problem.getnPeople(); i++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int j = 1; j < i; j++){
                lhs.addTerm(1, x[j-1][i-1]);
            }
            for(int j = i+1; j <= problem.getnPeople(); j++){
                lhs.addTerm(1, x[i-1][j-1]);
            }
            model.addEq(lhs, 1);
        }
        
    }
    
    public void solve() throws IloException{
        boolean has_feasible = model.solve();
        if(has_feasible){
            System.out.println("Optimal Objective = "+model.getObjValue());
        }else{
            System.out.println("No feasible solution found.");
        }
    }
    
    public void printSolution() throws IloException{
        for(int i = 1; i <= problem.getnPeople()-1; i++){
            for(int j = i+1; j <= problem.getnPeople(); j++){
                System.out.println("Match "+i+"-"+j+" "+model.getValue(x[i-1][j-1]));
                
            }   
        }
    }
    
}
