package models;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.cplex.IloCplex;
import java.util.List;
import problems.CuttingStockProblem;

public class CuttingStockProblemModelPatterns{
    private final IloCplex model;
    private final CuttingStockProblem problem;
    private final IloIntVar[] x;

    /**
     * Creates the pattern-based optimization model for an instance of the Cutting Stock Problem.
     * @param problem
     * @throws IloException 
     */
    public CuttingStockProblemModelPatterns (CuttingStockProblem problem) throws IloException{
        this.problem = problem;
        this.model = new IloCplex();

        // First we create an int variable for each cutting pattern
        List<int[]> patterns = problem.getFeasibleCuttingPatterns();
        this.x = new IloIntVar[patterns.size()];
        // Then we populate the array
        for(int p = 1 ; p <= patterns.size() ; p++ ){
            x[p-1] = model.intVar(0,Integer.MAX_VALUE,"x_"+p); // remember arrays are populated from position 0 to n-1
        }
        

        // Objective funtion
        // We minimize the number of large rolls cut with each cutting pattern
        IloLinearNumExpr obj=model.linearNumExpr();
        for(int p = 1 ; p <= patterns.size() ; p++ ){
            obj.addTerm(1 , x[p-1]); 
        }
        model.addMinimize(obj);

        // Constraints
        
        // We should satisfy the demand of each small roll
        for(int i = 1 ; i <= problem.getnWidths() ; i++ ){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int p = 1 ; p <= patterns.size() ; p++ ){
                lhs.addTerm(patterns.get(p-1)[i-1],x[p-1]); 
            }
            model.addGe(lhs, problem.getDemand()[i-1]);
        }
    }
    /**
     * Solves the problem and reports its optimal objective value. 
     * @throws IloException 
     */
    public void solve() throws IloException{
       boolean has_feasible_solution = model.solve();
        if(has_feasible_solution){
            System.out.println("Optimal Objective value = "
                    + model.getObjValue());
        }else{
            System.out.println("No feasible solution was found.");
        }
    }	
    
    
}