package models;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.cplex.IloCplex;
import problems.CuttingStockProblem;

public class CuttingStockProblemModel{
    private final IloCplex model;
    private final CuttingStockProblem problem;
    private final IloIntVar[][] x;
    private final IloIntVar[] y;

    /**
     * Creates the optimization model for an instance of the Cutting Stock Problem.
     * @param problem
     * @throws IloException 
     */
    public CuttingStockProblemModel (CuttingStockProblem problem) throws IloException{
        this.problem = problem;
        this.model = new IloCplex();

        // First we create a binary variable for each possible large roll
        this.y = new IloIntVar[problem.getMaxNumberLargeRolls()];
        // Then we populate the array
        for(int j = 1 ; j <= problem.getMaxNumberLargeRolls() ; j++ ){
            y[j-1] = model.boolVar("y_"+j); // remember arrays are populated from position 0 to n-1
        }
        // Then we create integer variables indicating how many small rolls
        // of a kind to cut out of a large roll
        this.x = new IloIntVar[problem.getnWidths()][problem.getMaxNumberLargeRolls()];
        for(int i = 1; i <= problem.getnWidths(); i++){
            for(int j = 1 ; j <= problem.getMaxNumberLargeRolls() ; j++ ){
                x[i-1][j-1] = model.intVar(0, Integer.MAX_VALUE,"x_"+i+"-"+j);
            }
        }

        // Objective funtion
        // We minimize the number of large rolls
        IloLinearNumExpr obj=model.linearNumExpr();
        for(int j = 1 ; j <= problem.getMaxNumberLargeRolls() ; j++ ){
            obj.addTerm(1 , y[j-1]); 
        }
        model.addMinimize(obj);

        // Constraints
        
        // We should respect the width of the large rolls
        for(int j = 1 ; j <= problem.getMaxNumberLargeRolls() ; j++ ){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int i = 1 ; i <= problem.getnWidths() ; i++ ){
                lhs.addTerm(problem.getWidths()[i-1], x[i-1][j-1]); 
            }
            lhs.addTerm(-problem.getWidthLargeRolls(), y[j-1]);
            model.addLe(lhs, 0);
        }
        
        // We should satisfy the demand of each small roll
        for(int i = 1 ; i <= problem.getnWidths() ; i++ ){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int j = 1 ; j <= problem.getMaxNumberLargeRolls() ; j++ ){
                lhs.addTerm(1, x[i-1][j-1]); 
            }
            model.addGe(lhs, problem.getDemand()[i-1]);
        }

    }
    /**
     * Solves the problem and reports its optimal objective value. 
     * @throws IloException 
     */
    public void solve() throws IloException{
       boolean has_feasible_solution = model.solve();
        if(has_feasible_solution){
            System.out.println("Optimal Objective value = "
                    + model.getObjValue());
        }else{
            System.out.println("No feasible solution was found.");
        }
    }	
    
    /**
     * Prints the solution to the problem instance.
     * @throws IloException 
     */
    public void printSolution() throws IloException{
        for(int j = 1 ; j <= problem.getMaxNumberLargeRolls() ; j++ ){
            if(model.getValue(y[j-1]) > 0){
                System.out.println("Using large roll number "+j);
                System.out.println("Small rolls cut:");
                for(int i = 1 ; i <= problem.getnWidths() ; i++ ){
                    System.out.println(model.getValue(x[i-1][j-1])+ " of width "+problem.getWidths()[i-1]);
                }
            }
        }
    }
}