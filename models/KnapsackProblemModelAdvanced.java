package models;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.cplex.IloCplex;
import problems.KnapsackProblem;

public class KnapsackProblemModelAdvanced{
    private final IloCplex model;
    private final KnapsackProblem problem;
    private final IloIntVar[] x;
	
	
    public KnapsackProblemModelAdvanced (KnapsackProblem problem) throws IloException{
        this.problem = problem;
        this.model = new IloCplex();

        // Here we create an array of nItems decision variables
        // First we create the array of length n
        this.x = new IloIntVar[problem.getnItems()];
        // Then we populate the array
        for(int j = 1 ; j <= problem.getnItems() ; j++ ){
            x[j-1] = model.intVar(0,Integer.MAX_VALUE,"x_"+j); // remember arrays are populated from position 0 to n-1
        }

        // Objective funtion
        // For each item we add a term R_j*x_j
        IloLinearNumExpr obj=model.linearNumExpr();
        for(int j = 1 ; j <= problem.getnItems() ; j++ ){
            System.out.println("Term "+j+" "+problem.getReward(j));
            obj.addTerm(problem.getReward(j) , x[j-1]); // always remember that x is an array, so variable x_j is in position j-1 ...
        }
        model.addMaximize(obj);

        // Constraint (we have only one constraint)
        IloLinearNumExpr lhs = model.linearNumExpr();
        for(int j = 1 ; j <= problem.getnItems() ; j++ ){
            lhs.addTerm(problem.getWeight(j) , x[j-1]); // always remember that x is an array, so variable x_j is in position j-1 ...
        }
        model.addLe(lhs, problem.getCapacity());

    }
    // Solves the model
    public void solve() throws IloException{
        // Sets the parameters 
        // Time limit
        model.setParam(IloCplex.IntParam.TimeLimit, 1000);
        // Optimality gap
        model.setParam(IloCplex.DoubleParam.EpGap,0.1/100); 
        // We want to solve each node LP relaxation using primal simplex (for illustrative purposes) 
        model.setParam(IloCplex.IntParam.NodeAlg,IloCplex.Algorithm.Primal);
        // Tells the model to use the callback myCallback (defined later)
        model.use(new myCallback());
		
        model.solve();
        System.out.println("Objective "+model.getObjValue());
    }
    // Prints the solution
    public void printSolution() throws IloException{
        for(IloIntVar var : x){
            // getName() is a method of any IloNumVar (or subclasses) object
            System.out.println(var.getName()+" "+model.getValue(var));
        }		
    }
    // Prints the entire model
    public void printModel(){
        System.out.println(model.toString());
    }
	
    // Defines the callback. It is a class which extends the class IloCplex.MIPInfoCallback
    // particularly we make it a private class so that it can only be seen inside the enveloping class (KnapsackProblemModel)
    private class myCallback extends IloCplex.MIPInfoCallback{
		
        // A simple constructor, but we might also create more complicated ones
        public myCallback() {
        }

        // This class must necessarily have a main method, which is said to "override" the main of the IloCplex.MIPInfoCallback class
        @Override
        protected void main() throws IloException {
            // This method is called prior to solving each node in the branch and cut tree
            // We make it print some information
            System.out.println("Best integer:"+getIncumbentObjValue()+" best bound:"+ getBestObjValue()+" gap:"+ getMIPRelativeGap()*100+"% \n"+" Nodes processed:"+getNnodes()+" Remaining:"+getNremainingNodes());
            
        }
    }	
}