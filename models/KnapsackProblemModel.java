package models;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.cplex.IloCplex;
import problems.KnapsackProblem;

public class KnapsackProblemModel{
    private final IloCplex model;
    private final KnapsackProblem problem;
    private final IloIntVar[] x;

    /**
     * Creates the optimization model for an instance of the Knapsack Problem.
     * @param problem
     * @throws IloException 
     */
    public KnapsackProblemModel (KnapsackProblem problem) throws IloException{
        this.problem = problem;
        this.model = new IloCplex();

        // Here we create an array of nItems decision variables
        // First we create the array of length n
        this.x = new IloIntVar[problem.getnItems()];
        // Then we populate the array
        for(int j = 1 ; j <= problem.getnItems() ; j++ ){
            x[j-1] = model.intVar(0,Integer.MAX_VALUE,"x_"+j); // remember arrays are populated from position 0 to n-1
        }

        // Objective funtion
        // For each item we add a term R_j*x_j
        IloLinearNumExpr obj=model.linearNumExpr();
        for(int j = 1 ; j <= problem.getnItems() ; j++ ){
            obj.addTerm(problem.getReward(j) , x[j-1]); // always remember that x is an array, so variable x_j is in position j-1 ...
        }
        model.addMaximize(obj);

        // Constraint (we have only one constraint)
        IloLinearNumExpr lhs = model.linearNumExpr();
        for(int j = 1 ; j <= problem.getnItems() ; j++ ){
            lhs.addTerm(problem.getWeight(j) , x[j-1]); // always remember that x is an array, so variable x_j is in position j-1 ...
        }
        model.addLe(lhs, problem.getCapacity());

    }
    /**
     * Solves the problem and reports its optimal objective value. 
     * @throws IloException 
     */
    public void solve() throws IloException{
       boolean has_feasible_solution = model.solve();
        if(has_feasible_solution){
            System.out.println("Optimal Objective value = "
                    + model.getObjValue());
        }else{
            System.out.println("No feasible solution was found.");
        }
    }	
    
    /**
     * Prints the solution to the problem instance.
     * @throws IloException 
     */
    public void printSolution() throws IloException{
        for(IloIntVar var : x){
            // getName() is a method of any IloNumVar (or subclasses) object
            System.out.println(var.getName()+" "+model.getValue(var));
        }		
    }
}