package algebra;

/**
 * Creates a class representing a matrix.
 * @author lct495
 */
public class Matrix {
    
    private double[][] elements;
    
    /**
     * Creates a Matrix given its elements
     * @param elements the elements of the matrix
     */
    public Matrix(double[][] elements){
        this.elements = elements;
    }
    
    /**
     * Creates a diagonal Matrix given the elements on its diagonal.
     * @param diagonal the elements on its diagonal
     */
    public Matrix(double[] diagonal){
        // Note: when you have an array, array.length gives you the
        // length of the array. This is a case of an object with a
        // public state. 
        
        // First creates a square matrix having dimensions equal to the
        // length of the diagonal elements.
        elements = new double[diagonal.length][diagonal.length];
        
        // Then iterates through its elements by means of 
        // two for loops, one over the rows and another over the columns.
        for(int i = 1; i <= diagonal.length; i++){
            for(int j = 1; j <= diagonal.length; j++){
                // If the element of the matrix is on the diagonale (i = j)
                // puts the value passed in the diagonal vector, otherwise
                // it puts a zero.
                if(i != j){
                    elements[i-1][j-1] = 0.0;
                }else{
                    elements[i-1][j-1] = diagonal[i-1];
                }
                // Notice how the elements of the matrix and vector are 
                // accessed using i-1 and j-1.
            }
        }
    }
    /**
     * Creates the identity Matrix
     * @param dimension the dimension of the matrix
     */
    public Matrix(int dimension){
        if(dimension < 1){
            // Exceptions are objects which are useful for informing 
            // the user of the code that something unplanned happened. 
            // As an example, if the dimension of the matrix is smaller
            // than one we cannot create a matrix. Therefore we create
            // an object of the class "IllegalArgumentException" by passing
            // an informative string, and then "throw" it.
            // When we throw an exception it will be passed to the method
            // which invoked the current method, and then further up
            // unless it finds some code which "catches" the exception 
            // and handles it. In this case we did not write any code to handle 
            // the exception (it is an advanced topic), therefore, if we pass 
            // a dimension < 1 if will interrupt the execution of the code and 
            // show the message of the exception to the user. Give it a try. 
            // If you want to read more about exceptions have a look here
            // https://docs.oracle.com/javase/tutorial/essential/exceptions/index.html 
            // IllegalArgumentException is a class which represents 
            // exceptions caused by passing inappropriate arguments.
            // All the exceptions "extend" a class called Exception or one of
            // its subclasses (if you read the topic "inheritance" this will be 
            // more clear). 
            IllegalArgumentException e = new IllegalArgumentException("The dimension must be a strictly positive integer.");
            throw e;
        }
        // Notice that I am not using this.elements since there is no
        // ambiguity on which elements object I am referring to. 
        // It creates an empty matrix of appropriate dimensions. 
        elements = new double[dimension][dimension];
        
        // We iterate over its positions and put 1 on the diagonal
        // and zero elsewhere. 
        for(int i = 1; i <= dimension; i++){
            for(int j = 1; j <= dimension; j++){
                if(i != j){
                    elements[i-1][j-1] = 0.0;
                }else{
                    elements[i-1][j-1] = 1.0;
                }
            }
        }
    }
    /**
     * Checks if the matrix is diagonal.
     * @return a boolean equal to true if the matrix is diagonal, 
     * i.e., 0 everywhere except on the diagonal, false otherwise.
     */
    public boolean isDiagonal(){
        // We set a isDiagonal initially to true
        // assuming the matrix is actually diagonal.
        boolean isDiagonal = true;
        for(int i = 1; i <= elements.length; i++){
            for(int j = 1; j <= elements[i-1].length; j++){
                // As soon as we find a non-zero element outside
                // the diagonal we say that the matrix is not diagonal.
                if(i != j && elements[i-1][j-1] != 0 ){
                    isDiagonal = false;
                }
            }
        }
        return isDiagonal;
    }
    /**
     * Checks if the matrix is square.
     * @return true if the the matrix is square, false otherwise
     */
    public boolean isSquare(){
        // Returns true if the number of rows is equal to the number of columns.
        // It assumes (as we will always do) that all the columns have the same length,
        // so we onnly check the length of the first column.
        return elements.length == elements[0].length;
    }  
    
    /**
     * Multiplies the matrix by a given Matrix passed as parameter.
     * The operation can be performed only if the dimensions are congruent,
     * i.e., (m x n)*(n x p) = (m x p)
     * @param a a matrix
     * @return the product matrix
     */
    public Matrix multiply(Matrix a){
        // Calls the method getNColumns() of the current object
        // and the method getNRows() of the matrix which should be multiplied.
        if(this.getNColumns() != a.getNRows()){
            // If columns and rows are not compatible it throws an exception.
            throw new IllegalArgumentException("Incompatible dimensions.");
        }else{
            // Creates an empty 2-dimensiona array which will hold the result 
            // of the multiplication
            double[][] resultElements = new double[getNRows()][a.getNColumns()];
            
            // For each row in the present matrix
            for(int i = 1; i <= this.getNRows(); i++){
                // For each column in the a matrix
                for(int k = 1; k <= a.getNColumns(); k++){
                    // defines the product initially as zero
                    double product = 0;
                    // for each element in the row of the present matrix and 
                    // column of the a matrix, it sums the products
                    for(int j = 1; j <= this.getNColumns(); j++){
                        product+= elements[i-1][j-1] * a.getElements()[j-1][k-1];
                    }
                    // Puts the result in the new matrix
                    resultElements[i-1][k-1] = product;
                }
            }
            // Finally returns the new matrix
            return new Matrix(resultElements);
        }
    }
    /**
     * Performs a scalar multiplication. 
     * @param scalar the scalar to multiply the matrix by
     * @return the Matrix resulting from the scalar product of this matrix and 
     * a scalar provided as parameter
     */
    public Matrix multiply(double scalar){
        
        double[][] resultElements = new double[this.getNRows()][this.getNColumns()];
        for(int i = 1; i <= getNRows(); i++){
            for(int j = 1; j <= getNColumns(); j++){
                resultElements[i-1][j-1] = elements[i-1][j-1]*scalar;
            }
        }
        return new Matrix(resultElements);
    }
        
    /**
     * Prints the elements of the matrix.
     */
    public void print(){
        System.out.println("*****************");
        for(int i = 1; i <= elements.length; i++){
            // elements[i-1].length gives you the length of the 
            // row i-1. In principle we could have rows of different 
            // lengths, but in practice you hardly see it done. 
            for(int j = 1; j <= elements[i-1].length; j++){
                // System.out.print() prints what you pass without occupying an entire line
                // System.out.println() prints what you pass, completes the rest of the line
                // with blanks and moves the cursor to the a new line. 
                System.out.print(elements[i-1][j-1]+ " ");
            }
            System.out.println("");
        }
        System.out.println("*****************");
        // Do you want to format your output in a nicer way?
        // Have a look the static method "format" of the class "String"
        // https://docs.oracle.com/javase/8/docs/api/java/lang/String.html#format-java.lang.String-java.lang.Object...- 
        // Or in general at all the methods of the class String
        // https://docs.oracle.com/javase/8/docs/api/java/lang/String.html        
    }
    /**
     * Returns the number of rows.
     * @return 
     */
    public int getNRows(){
        return elements.length;
    }
    /**
     * Returns the number of columns.
     * @return 
     */
    public int getNColumns(){
        return elements[0].length;
    }
    /**
     * Returns the elements of the matrix.
     * @return 
     */
    public double[][] getElements() {
        return elements;
    }
    
    
}
