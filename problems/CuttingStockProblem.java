/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problems;

import commons.CartesianProduct;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.math3.util.Combinations;

/**
 * This class represents instances of the Cutting Stock problem.
 * @author lct495
 */
public class CuttingStockProblem {
    private final int nWidths;
    private final double widthLargeRolls;
    private final double widths[];
    private final double demand[];

    public CuttingStockProblem(int nWidths, double widthLargeRolls, double[] widths, double[] demand) {
        if(widths.length != nWidths || demand.length != nWidths){
            throw new IllegalArgumentException("Invalid data.");
        }
        this.nWidths = nWidths;
        this.widthLargeRolls = widthLargeRolls;
        this.widths = widths;
        this.demand = demand;
    }

    public int getnWidths() {
        return nWidths;
    }

    public double getWidthLargeRolls() {
        return widthLargeRolls;
    }

    public double[] getWidths() {
        return widths;
    }

    public double[] getDemand() {
        return demand;
    }
    /**
     * Returns an upper bounds on the number of large rolls to use.
     * @return 
     */
    public int getMaxNumberLargeRolls(){
        // A (loose) upprt bound is given by the sum of the demands
        int nRolls = 0;
        for(int i = 1; i <= nWidths; i++){
            nRolls+= demand[i-1];
        }
        return nRolls;
    }
    /**
     * Returns all feasible cutting patterns.
     * Cutting patterns are returned in a List (specifically an ArrayList)
     * see https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html.
     * Specifically, a list whose elements are arrays of integer (notice the <int[]>).
     * We use lists instead of arrays for the following reason.We do not know
     * the number of feasible cutting patterns beforehand. For this reason we cannot
     * use an array (since we need to initialize it with its size). Instead, lists
     * allow adding new elements without specifying the total size. 
     * ArrayList is an implementation of List, which is an Abstract class (see
     * the Java tutorial for more information on Abstract classes).
     * @return 
     */
    public List<int[]> getFeasibleCuttingPatterns(){
        // We are creating a list whose elements are int[].
        // If we add elements that are not int[] it raises an exception.
        // Notice that we use ArrayLists as a specific implementation of a List.
        List<int[]> feasiblePatterns = new ArrayList();
        
        // Each pattern is a array of integer (int[]) of length
        // equal to the number of widths (nWidths). Each element of 
        // the array represents the number of small rolls of the corresponding
        // width cut with the specific cutting pattern.
        // E.g. [3,2,0] means that we cut 3 rolls of the first width, 2 of the second,
        // 0 of the third. 
        
        // First, we find how many rolls at most we can cut of each width from 
        // a single large roll. This number will be floor(widthLargeRolls/widthSmallRoll).
        // We take the highest number.
        double maxCuts[] = new double[nWidths];
        for(int i = 1; i <= nWidths; i++){
            maxCuts[i-1] = Math.floor(widthLargeRolls / widths[i-1]);
            System.out.println("We can cut at most "+maxCuts[i-1]+" of width "+widths[i-1]);
        }
        
        
        // Now, to generate all possible patterns, and then we check which ones are feasible.
        // All possible patterns are given by the Cartesian product of the integers up to maxCuts.
        // That is, if we can cut 
        // -- up to 2 of width w1, that is 0,1 or 2
        // -- up to 2 of width w2, that is 0,2 or 2
        // -- up to 3 of width w3, that is 0,1,2 or 3
        // all the possible patters are given by the Cartesian product of
        // the vectors [0,1,2] x [0,1,2] x [0,1,2,3]
        // Therefore, we create these vectors, and we add it to a List that we
        // pass to an object of class CartesianProduct.
        CartesianProduct cp = new CartesianProduct();
        List<int[]> vectors = new ArrayList();
        for(int w = 1; w <= nWidths; w++){
            int vector[] = new int[(int)maxCuts[w-1]+1];
            for(int i = 0; i <= maxCuts[w-1]; i++){
                vector[i] = i;
            }
            vectors.add(vector);
        }
        List<int[]> product = cp.product(vectors);
        
        // Now we have all the possible cutting patterns and we need to find the 
        // feasible ones
        feasiblePatterns = new ArrayList();
        
        for(int[] pattern : product){
            // A pattern is feasible if it does not violate the width of the 
            // large rolls
            double widthCut = 0;
            for(int w = 1; w <= nWidths; w++){
                widthCut += widths[w-1] * pattern[w-1];
            }
            if(widthCut <= widthLargeRolls){
                feasiblePatterns.add(pattern);
                System.out.println("Added feasible pattern");
                for(int w = 1; w <= nWidths; w++){
                    System.out.println(pattern[w-1]);
                }
                System.out.println("");
            }
        }

        return feasiblePatterns;
    }
    
    
    public void printSummary(){
        System.out.println("Small rolls of "+nWidths+" possible widths:");
        for(int i = 1; i <= nWidths; i++){
            System.out.println(widths[i-1]);
        }
        System.out.println("to be cut out of large rolls of witht "+widthLargeRolls+".");
        System.out.println("At most "+getMaxNumberLargeRolls()+" large rolls will be used.");
        System.out.println("The demand of each width is:");
        for(int i = 1; i <= nWidths; i++){
            System.out.println(demand[i-1]);
        }
    }
    
    
    
}
