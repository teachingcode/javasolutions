package optimizationProblems;

/**
 * This class provides a template for the Diet Problem. 
 * Instances (that is objects) of this class represent 
 * instances of the Diet Problem. Note: objects of 
 * this class represent instances of the real-life 
 * problem, identified by a specific data set. 
 * That is, they do not represent the associated 
 * mathematical problem. 
 * @author lct495
 */
public class DietProblem {
    
    // Every instance of the diet problem 
    // has a number of foods, a number of nutrients 
    // a cost for each food, a requirement intake
    // for each nutrient, and the composition of the 
    // foods in terms of the nutrients. 
    private final int nFoods;
    private final int nNutrients;
    private final double costs[];
    private final double foodsComposition[][];
    private final double nutrientsRequirement[];
    private final String foodNames[];
    private final String nutrientNames[];
    // Note: I have used the keyword "final". This is used when 
    // the value of the field (instance or static variables)
    // is set once and never changed again. In this 
    // particular class it is appropriate as we set the values 
    // the constructor and we do not have methods which 
    // can modify them. 
    
    // Note that foodNames and nutrientNames are only useful
    // for keeping track of the names. However,
    // for the sake of building and solving an optimization problem
    // they are irrelevant. 
    
    // Constructors
    /**
     * Constructor of a Diet Problem from its data.
     * @param nFoods
     * @param nNutrients
     * @param costs
     * @param foodsComposition
     * @param nutrientsRequirement 
     * @param foodNames 
     * @param nutrientNames 
     */
    public DietProblem(int nFoods, int nNutrients, double costs[], double foodsComposition[][], double nutrientsRequirement[],String foodNames[],String nutrientNames[]) {
        this.nFoods = nFoods;
        this.nNutrients = nNutrients;
        this.costs = costs;
        this.foodsComposition = foodsComposition;
        this.nutrientsRequirement = nutrientsRequirement;
        this.foodNames = foodNames;
        this.nutrientNames = nutrientNames;
    }
    
    // Methods 
    // Note that not all methods are strictly necessary,
    // I have added some of them only for illustrative purposes.
    /**
     * Returns the number of foods.
     * @return 
     */
    public int getNFoods() {
        return nFoods;
    }
    /**
     * Returns the number of nutrients.
     * @return 
     */
    public int getNNutrients() {
        return nNutrients;
    }
    /**
     * Returns the array of costs.
     * @return 
     */
    public double[] getCosts() {
        return costs;
    }
    public String getFoodName(int food){
        if(food < 1 || food > nFoods){
            throw new IllegalArgumentException("The food number must be in [ 1,"+nFoods+" ]");
        }
        return foodNames[food-1];
    }
    public String getNutrientName(int nutrient){
        if(nutrient < 1 || nutrient > nNutrients){
            throw new IllegalArgumentException("The nutrient number must be in [ 1,"+nNutrients+" ]");
        }
        return nutrientNames[nutrient-1];
    }
    /**
     * Returns the matrix of food compositions.
     * @return 
     */
    public double[][] getFoodsComposition() {
        return foodsComposition;
    }
    /**
     * Returns the array of nutrients requirement.
     * @return 
     */
    public double[] getNutrientsRequirement() {
        return nutrientsRequirement;
    }
    /**
     * Returns the content of a nutrient is a food.
     * @param food in [1 , nFoods]
     * @param nutrient in [1 , nNutrients]
     * @return 
     */
    public double getFoodsComposition(int food, int nutrient) {
        if(nutrient < 1 || nutrient > nNutrients){
            throw new IllegalArgumentException("The nutrient number must be in [ 1,"+nNutrients+" ]");
        }
        if(food < 1 || food > nFoods){
            throw new IllegalArgumentException("The food number must be in [ 1,"+nFoods+" ]");
        }
        return foodsComposition[food-1][nutrient-1];
    }
    /**
     * Returns the requirement of a food.
     * @param nutrient in [1,nNutrients]
     * @return 
     */
    public double getNutrientsRequirement(int nutrient) {
        if(nutrient < 1 || nutrient > nNutrients){
            throw new IllegalArgumentException("The nutrient number must be in [ 1,"+nNutrients+" ]");
        }
        return nutrientsRequirement[nutrient-1];
    }
    
    /**
     * Returns the cost of a given food i
     * @param food the food in [1,nFoods]
     * @return the cost
     */
    public double getCost(int food){
        if(food < 1 || food > nFoods){
            throw new IllegalArgumentException("The food number must be in [ 1,"+nFoods+" ]");
        }
        return costs[food-1];
    }
    
    public void print(){
        System.out.println("*******************");
        System.out.println("Diet Problem ");
        System.out.println("*******************");
        System.out.println("# Foods : "+nFoods);
        System.out.println("# Nutrients : "+ nNutrients);
        System.out.println("Costs :");
        for(int i = 1 ; i <= nFoods; i++){
            System.out.print("#"+i+" "+foodNames[i-1]+" "+costs[i-1]+" ");
        }
        System.out.println(" ");
        System.out.println("Requirements : ");
        for(int j = 1; j <= nNutrients; j++){
            System.out.print("#"+j+" "+nutrientNames[j-1]+" "+nutrientsRequirement[j-1]+ " ");
        }
        System.out.println(" ");
        
        System.out.println("Foods composition :");
        for(int i = 1 ; i <= nFoods; i++){
            for(int j = 1; j <= nNutrients; j++){
                System.out.print(foodsComposition[i-1][j-1]+ " ");
            }
            System.out.println(" ");
        }
    }
}
