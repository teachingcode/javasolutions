/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problems;

/**
 *
 * @author lct495
 */
public class MatchingProblem {
    
    private int nPeople;
    private double[][] quality;

    public MatchingProblem(int nPeople, double[][] quality) {
        if(quality.length != nPeople || quality[0].length != nPeople){
            throw new IllegalArgumentException("Invalid matrix dimensions.");
        }
        this.nPeople = nPeople;
        this.quality = quality;
    }

    public int getnPeople() {
        return nPeople;
    }
    public double getQuality(int i, int j){
        if(i <= 0 || i > quality.length || j <= 0 || j > quality.length ){
            throw new IllegalArgumentException("Invalid i-j pair");
        }
        return quality[i-1][j-1];
    }
    public void printSummary(){
        for(int i = 1; i <= nPeople; i++){
            for(int j = 1; j <= nPeople; j++){
                System.out.print(quality[i-1][j-1]+" ");
            }
            System.out.println(" ");
        }
    }

    
    
}
