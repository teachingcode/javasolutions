package problems;

public class KnapsackProblem{
    private final int nItems;
    private final double[] rewards;
    private final double[] weights;
    private final double capacity;

    public KnapsackProblem(int nItems, double[] rewards, double[] weights, double capacity) {
        this.nItems = nItems;
        this.rewards = rewards;
        this.weights = weights;
        this.capacity = capacity;
    }

    public int getnItems() {
        return nItems;
    }

    public double[] getRewards() {
        return rewards;
    }
    public double getReward(int i){
        if(i <= 0 || i > rewards.length){
            throw new IllegalArgumentException("Item "+i+" must be between 1 and "+rewards.length);
            
        }
        return rewards[i-1];
    }
    
    public double[] getWeights() {
        return weights;
    }
    
    public double getWeight(int i){
        if(i <= 0 || i > weights.length){
            throw new IllegalArgumentException("Item "+i+" must be between 1 and "+weights.length);
            
        }
        return weights[i-1];
    }

    public double getCapacity() {
        return capacity;
    }
    
    public void printSummary(){
        System.out.println("Instance of the Knapsack Problem with:");
        System.out.println(nItems+" items");
        System.out.println("Capacity "+capacity);
        System.out.println("Items :");
        for(int i =1; i <= nItems; i++){
            System.out.println(i+" reward "+rewards[i-1]+" weight "+weights[i-1]);
        }
        
    }
    
	
	
	
		
}