/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commons;

/**
 *
 * @author lct495
 */

import java.util.ArrayList; 
import java.util.List;
 
public class CartesianProduct {
    
    public List<int []> product(List<int[]> vectors){
        //List<int[]> vectorsOfProduct = productOfOne(vectors.get(0));
        List<int[]> vectorsOfProduct = productOfOne(vectors.get(vectors.size()-1));
        for(int i = vectors.size()-2; i >= 0; i--){
            vectorsOfProduct = productOfTwo(vectors.get(i),vectorsOfProduct);
        }
        return vectorsOfProduct;
    }
    
    private List<int[]> productOfOne(int[] a){
        List<int[]> vectorsOfProduct = new ArrayList();
        for(int i: a){
            int[] e = new int[1];
            e[0] = i;
            vectorsOfProduct.add(e);
        }  
        return vectorsOfProduct;
    }
    private List<int[]> productOfTwo(int[] a, List<int[]> p){
        
        List<int[]> vectorsOfProduct = new ArrayList();
        // Calculates the size of the new product
        int newSize = p.get(0).length + 1;
        for(int i = 1; i <= a.length; i++){
            int firstElement = a[i-1];
            for(int j = 1; j <= p.size(); j++){
                int[] lastElements = p.get(j-1);
                int[] newElement = new int[newSize];
                newElement[0] = firstElement;
                for(int k = 1; k < newSize; k++){
                    newElement[k] = lastElements[k-1];
                }
                vectorsOfProduct.add(newElement);
            }
        }
        return vectorsOfProduct;
    }
}

