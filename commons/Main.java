/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commons;

import algebra.Matrix;
import ilog.concert.IloException;
import models.CuttingStockProblemModel;
import models.CuttingStockProblemModelPatterns;
import models.KnapsackProblemModel;
import models.KnapsackProblemModelAdvanced;
import models.MatchingProblemModel;
import optimizationProblems.DietProblem;
import optimizationProblems.DietProblemModel;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import problems.CuttingStockProblem;
import problems.KnapsackProblem;
import problems.MatchingProblem;

/**
 *
 * @author lct495
 */
public class Main {

    public static void main(String[] args) throws IloException{
        // ****************************
        // Exercise 3: Diet Problem
        // ****************************
        
        // SOME EXPERIMENTS WITH THE DIET PROBLEM 
        // First we create the data that then we pass to 
        // the constructor of the Diet Problem
        int nFoods = 5;
        int nNutrients = 3;
        // Notice this alternative way of populating an array
        double costs[] = {8,10,3,20,15};
        String foodNames[] = {"Apples","Bananas","Carrots","Dates","Eggs"};
        double nutrientsRequirement[] = {70,50,12};
        String nutrientNames[] = {"Protein","Vitamin","Iron"};
        
        double foodsComposition[][] = new double[nFoods][nNutrients];
        foodsComposition[0][0] = 0.4;
        foodsComposition[0][1] = 6;
        foodsComposition[0][2] = 0.4;
        
        foodsComposition[1][0] = 12;
        foodsComposition[1][1] = 10;
        foodsComposition[1][2] = 0.6;
        
        foodsComposition[2][0] = 6;
        foodsComposition[2][1] = 3;
        foodsComposition[2][2] = 0.4;
        
        foodsComposition[3][0] = 6;
        foodsComposition[3][1] = 1;
        foodsComposition[3][2] = 0.2;
        
        foodsComposition[4][0] = 12.2;
        foodsComposition[4][1] = 0;
        foodsComposition[4][2] = 2.6;
        
        DietProblem dp = new DietProblem(nFoods, nNutrients, costs, foodsComposition, 
                nutrientsRequirement, foodNames, nutrientNames);
        dp.print();
        
        System.out.println("The content of Vitamin in Carrots is "+dp.getFoodsComposition(3, 2));
        
        // EXPERIMENTS WITH THE DietProblemModel CLASS.
        
        // We build a DietProblemModel using its constructor.
        // The constructor receives an instance of the DietProblem class.
        DietProblemModel model = new DietProblemModel(dp);
        // Notice that DietProblemModel can throw exceptions (IloException), both at construction,
        // when solving the problem, and accessing its solution (primal or dual).
        // Therefore we need to state that also the Main method can throw 
        // IloExceptions when opening the main method. 
        
        // We solve the problem
        model.solve();
        
        // We print its solution
        model.printSolution();
        
        // We print its dual solution
        model.printDuals();
        
        // We print the model itself
        model.print();
        
        // ****************************
        // Exercise 2: Algebra
        // ****************************
        
        // Creates an arbitrary array of numbers
        double diagonal[] = new double[5];
        diagonal[0] = 2;
        diagonal[1] = 7.5;
        diagonal[2] = 1E-2; // What does this mean? Check it out ..
        diagonal[3] = -0.25;
        diagonal[4] = -2.5 *100;
        
        // Creates a diagonal matrix
        Matrix m1 = new Matrix(diagonal);
        m1.print();
        
        // Create an identity matrix
        Matrix m2 = new Matrix(4);
        m2.print();
        
        // Creates a random non-square matrix
        double elements[][] = new double[10][15];
        for(int i = 1; i <= 10; i++){
            for(int j = 1; j <= 15; j++){
                // The static method random() of the class Math (in the JDK)
                // provides a random number in [0,1)
                elements[i-1][j-1] = Math.random();
            }
        }
        Matrix m3 = new Matrix(elements);
        m3.print();
        
        // Checks if the three matrices are diagonal
        System.out.println("Is m1 diagonal ? "+m1.isDiagonal());
        System.out.println("Is m2 diagonal ? "+m2.isDiagonal());
        System.out.println("Is m3 diagonal ? "+m3.isDiagonal());
        
        // Checks if the three matrices are square
        System.out.println("Is m1 square ? "+m1.isSquare());
        System.out.println("Is m2 square ? "+m2.isSquare());
        System.out.println("Is m3 square ? "+m3.isSquare());
        
        // Creates a new diagonal matrix 
        // Creates an arbitrary array of numbers
        double diagonal2[] = new double[5];
        diagonal2[0] = 200;
        diagonal2[1] = 17.5;
        diagonal2[2] = 1E2;
        diagonal2[3] = -1.25;
        diagonal2[4] = -2.5 * -100;
        
        Matrix m4 = new Matrix(diagonal2);
        
        // Multiplies m1 and m4 (they are of comformable dimension)
        Matrix m5 = m1.multiply(m4);
        m5.print();
        
        // Matrix m1 cannot be multiplied to matrix m2
        // because they are of different dimensions.
        // Try to run the following:
        // m1.multiply(m2);
        
        
        // Scalar multiplication of a couple of matrices
        Matrix m6 = m3.multiply(3);
        m6.print();
        
        Matrix m7 = m2.multiply(8);
        m7.print();
        
        // ****************************
        // Exercise 7: Knapsack Problem
        // ****************************
        
        // First, we create the data of the Knapsack Problem
        double knapsackCapacity = 450;
        int nItems = 100;
        double rewards[] = new double[nItems];
        double weights[] = new double[nItems];
        
        // We populate the arrays of rewards and weights
        // using the Uniform and Normal distribution from the Commons
        // Math library
        UniformRealDistribution ud = new UniformRealDistribution(100,120);
        NormalDistribution nd = new NormalDistribution(60,7);
        for(int i = 1; i <= nItems; i++){
            rewards[i-1] = ud.sample();
            weights[i-1] = nd.sample();
        }
        // Now that we have all the data, we can create an instance of the KnapsackProblem class
        KnapsackProblem kp = new KnapsackProblem(nItems,rewards,weights,knapsackCapacity);
        kp.printSummary();
        KnapsackProblemModel kpm = new KnapsackProblemModel(kp);
        kpm.solve();
        kpm.printSolution();
        KnapsackProblemModelAdvanced kpma = new KnapsackProblemModelAdvanced(kp);
        kpma.solve();
        kpma.printSolution();
        
        // ****************************
        // Exercise 6: Matching Problem
        // ****************************
        int nPeople = 10;
        double matchQuality[][] = new double[nPeople][nPeople];
        ud = new UniformRealDistribution(20,80);
        for(int i = 1; i <= nPeople-1; i++){
            for(int j = i+1; j <= nPeople; j++){
                matchQuality[i-1][j-1] = ud.sample();
                matchQuality[j-1][i-1] = matchQuality[i-1][j-1];
            }    
        }
        MatchingProblem mp = new MatchingProblem(nPeople,matchQuality);
        mp.printSummary();
        MatchingProblemModel mpm = new MatchingProblemModel(mp);
        mpm.solve();
        mpm.printSolution();
        
        // ****************************
        // Exercise 8: Cutting Stock Problem
        // ****************************
        int nWidths = 3;
        double widths[] = {2.1,1.8,1.5};
        double demands[] = {9,12,19};
        double widthLargeRolls = 5;
        
        CuttingStockProblem csp = new CuttingStockProblem(nWidths,widthLargeRolls,widths,demands);
        csp.printSummary();
        CuttingStockProblemModel cspm = new CuttingStockProblemModel(csp);
        cspm.solve();
        cspm.printSolution();
        csp.getFeasibleCuttingPatterns();
        CuttingStockProblemModelPatterns cpp = new CuttingStockProblemModelPatterns(csp);
        cpp.solve();
        
    }
    
}
